import java.util.ArrayList;
import java.util.Scanner;

class Mahasiswa {
    private String nama;
    private String nim;
    private String alamat;

    public Mahasiswa(String nama, String nim, String alamat) {
        this.nama = nama;
        this.nim = nim;
        this.alamat = alamat;
    }

    public String getNama() {
        return nama;
    }

    public String getNim() {
        return nim;
    }

    public String getAlamat() {
        return alamat;
    }
}

class DataMahasiswa {
    static ArrayList<Mahasiswa> mhsList = new ArrayList<>();

    public static void tambahData(Mahasiswa mahasiswa) {
        mhsList.add(mahasiswa);
    }

    public static void tampilData() {
        for (Mahasiswa mhs : mhsList) {
            System.out.printf("%15s | %-15s | %-15s %n",mhs.getNim(),mhs.getNama(),mhs.getAlamat());
        }
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean next = true;
        while (next) {
            System.out.print("masukkan nim : ");
            String nim = scanner.nextLine();

            System.out.print("masukkan nama : ");
            String nama = scanner.nextLine();

            System.out.print("masukkan alamat: ");
            String alamat = scanner.nextLine();

            Mahasiswa mahasiswa1 = new Mahasiswa(nama, nim, alamat);
            DataMahasiswa.tambahData(mahasiswa1);

            System.out.print("tambah lagi? ");
            String tambah = scanner.nextLine();

            if (tambah.equals("t")) {
                next = false;
            }
        }
        System.out.println("==================================");
        System.out.println("         ===Tampilan Data Mahasiswa UB===");
        DataMahasiswa.tampilData();
    }
}
